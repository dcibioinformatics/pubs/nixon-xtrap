\documentclass[8pt,compress]{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{graphicx}
\usetheme{Madrid}

\newcommand{\R}{\texttt{R}}
\newcommand{\lnk}[2]{\hyperlink{#1<1>}{\beamerreturnbutton{#2}} } % Link to slides by label

%% Alternate "Return Button" footline
\defbeamertemplate{footline}{return}{%
\parbox{\linewidth}{\vspace*{-10pt}\hfill\lnk{ind}{Return}\hfill\insertpagenumber/\insertpresentationendpage}}

%% Alternate "Return Button" footline for PFS
\defbeamertemplate{footline}{preturn}{%
\parbox{\linewidth}{\vspace*{-10pt}\hfill\lnk{pind}{Return}\hfill\insertpagenumber/\insertpresentationendpage}}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]


\begin{document}
<<r setup1, include=FALSE>>=
library(knitr)
knitr::opts_chunk$set(fig.path='figure/beamer-',
                      fig.align='center',
                      fig.show='hold',
                      size='tiny',
                      fig.width=10,
                      fig.height=6,
                      out.width='.9\\linewidth')
@

<<setup2, include=FALSE>>=
defaultWidth <- 75              # use to switch between widths for different size code output
tinyWidth <- 150                # use to switch between widths for different size code output
options(width=tinyWidth)        # make the printing fit on the page
opts_chunk$set(tidy = TRUE,
               tidy.opts=list(keep.blank.line=FALSE, width.cutoff=60)) # make code stay in frame
set.seed(1123)                  # make the results repeatable
stdt <- date()
@
  
\begin{frame}
  \title{XTRAP's Study Preprocessing}
  %\date{}
  % Date: Aug 12, 2019
  \author{Jing Lyu}
  \titlepage
\end{frame}

\begin{frame}{Links to Sections}
  \tableofcontents[]
\end{frame}
%#====================================================================================#
%#=======================          Load Libraries         ==============================#
%#====================================================================================#
\section{Libraries}
\begin{frame}[fragile,label=libs]{Libraries}
<<libs, size='tiny', message = FALSE, error = FALSE, warning = FALSE>>=
library(gdata)
library(tidyverse)
#library(xtable)            # used for creating latex tables from R object
library(readxl)
library(gridExtra)
library(ggpubr)
library(reshape2)
library(data.table)
library(BlandAltmanLeh)
@
\end{frame}

%#==========================================================================================#
%#============================             Load  Data         ==============================#
%#==========================================================================================#
\section{Load Data}
\begin{frame}[fragile]{Locate the data}
Set the location of the data files and location for output results.
<<locate,size= 'tiny'>>=
dir <-  "/data1/workspace/DCI/Nixon/XTRAP/Data/"
savdir <- "/data1/workspace/DCI/Nixon/XTRAP/Results/"
figdir <- "/data1/workspace/DCI/Nixon/XTRAP/Results/"
fid <- file.path(dir, "ID_correction.csv")
fname <- file.path(dir,"XTRAP-stats ready-070919.xlsx")
fclinic <- file.path(dir,"XTRAP clinical data for biomarker analyses 1Aug17.xlsx")
tools::md5sum(fid)
tools::md5sum(fname)
tools::md5sum(fclinic)
@
\end{frame}

\begin{frame}[fragile]{Marker Names}
<<marker,size= 'tiny'>>=
sheetnames <- excel_sheets(fname)   # Get sheet names
sheetnames
marknames <- gsub("-| ","",sheetnames)    #Get marker names without dashes or spaces
marknames
@
We have 49 biomarkers.
\end{frame}

\section{Read in Biomarker Data}
\begin{frame}[fragile]{Read in biomarker data and calculate final concentration}
<<readin1, size= 'tiny',message = FALSE, warning = FALSE>>=
trdata<-function(fname,sheetName){
  markName<-gsub("-| ","",sheetName)
  temp <- read_xlsx(fname,sheet=sheetName,na=".")
  temp <- temp %>% filter_at(vars(starts_with("Repl")),any_vars(!is.na(.))) #Remove NAs and empty rows
  newname <- paste0(markName,"_Final")
  temp1 <- temp %>%
    mutate_at(vars(starts_with("Repl")),funs(as.numeric)) %>%
    mutate(!!newname:=rowMeans(select(.,starts_with("Repl")),na.rm=T)*Dilution) %>%
    rename_at(vars(starts_with("Repl")), funs(paste0(markName,str_replace(.,"Repl ","_Rep"))))
  if ("Sample I.D."%in%colnames(temp1)){
    temp2 <- temp1 %>% separate(`Sample I.D.`,c("Patient ID","Date","Sample Des"),sep = "-") %>% 
      select("Patient ID",Date,"Sample Des",matches("Rep|Final")) %>%
      mutate(`Sample Des`=ifelse(grepl("BL",`Sample Des`),"Baseline",ifelse(grepl("PROG",`Sample Des`),"Progression",ifelse(grepl("C12",`Sample Des`),"C12D1",ifelse(grepl("C4",`Sample Des`),"C4D1","UNKOWN")))),Date=as.Date(Date,"%m/%d/%y"))
  }else{
    temp2 <- temp1 %>% select("Patient ID",Date,"Sample Des",matches("Rep|Final")) %>%
      mutate(Date=as.Date(Date))
  }
  temp3 <- id_list %>% right_join(temp2, by=c("oldid"="Patient ID")) %>%
  select(-oldid) %>% dplyr::rename(`Patient ID`=newid)  #Correction of IDs
  return(temp3)
}
id_list <- read.csv(fid)
mydata1 <- lapply(sheetnames,function (x) trdata(fname,x))
mydata2 <- mydata1%>%purrr::reduce(full_join,by=c("Patient ID","Date","Sample Des"))
as.character(unique(mydata2$`Patient ID`))
@
65 patients were included in biomarker records.
\end{frame}

\section{Read in Clinical Data}
\begin{frame}[fragile]{Read in clinical data}
Read in clinical data
<<readin2, size= 'tiny',message = FALSE, warning = FALSE>>=
clinc <- read_xlsx(fclinic ,na=".")
#head(clinc)
unique(clinc$pt)
table(clinc$`Treatment Assignment Code at Enrollment`)
@
63 patients in clincial data: 7 from Cohort 1, 6 from Cohort 2 and 50 from Phase II.
\end{frame}

\section{Combine Clinical Data and Biomarker Data}
\begin{frame}[fragile]{Combine clinical data and biomarker data}
<<comb, size= 'tiny',message = FALSE, warning = FALSE>>=
setdiff(mydata2$`Patient ID`,clinc$pt)
setdiff(clinc$pt,mydata2$`Patient ID`)
XTRAP_comb<-clinc %>% mutate(pt=as.character(pt)) %>%
  full_join(mydata2,by=c("pt"="Patient ID"))
#dim(XTRAP_comb)
length(unique(XTRAP_comb$pt))
@
Patient "313", "402", and "152/404" have marker records and don't have clinical data.
Patient "208" has clinical data and don't have marker records.
Finally, 66 patients are included in the combined data.
\end{frame}

<<Lr, size='tiny',message = FALSE,warning=FALSE, echo=FALSE>>=
XTRAP_comb1 <- XTRAP_comb %>% select(-matches("Rep")) %>% rename("Time"="Sample Des")
#Long to Wide
XTRAP_comb11 <- XTRAP_comb1 %>% filter(Time=="Baseline") %>% select("pt", paste0(marknames,"_Final")) %>% `colnames<-` (c("pt",paste0(marknames,"_Final_Baseline")))
XTRAP_comb12 <- XTRAP_comb1 %>% filter(Time=="C4D1") %>% select("pt", paste0(marknames,"_Final")) %>% `colnames<-` (c("pt",paste0(marknames,"_Final_C4D1")))
XTRAP_comb13 <- XTRAP_comb1 %>% filter(Time=="Progression") %>% select("pt", paste0(marknames,"_Final")) %>% `colnames<-` (c("pt",paste0(marknames,"_Final_Progression")))
XTRAP_comb14 <- XTRAP_comb1 %>% filter(Time=="30Day Post") %>% select("pt", paste0(marknames,"_Final")) %>% `colnames<-` (c("pt",paste0(marknames,"_Final_30DayPost")))

XTRAP_comb3 <- XTRAP_comb1 %>% filter(!duplicated(pt)) %>% 
  select(pt,osevent,osmonths,pfsevent,pfsmonths,`Response Assessment`) %>%
  full_join(XTRAP_comb11, by="pt") %>% 
  full_join(XTRAP_comb12, by="pt") %>%
  full_join(XTRAP_comb13, by="pt") %>%
  full_join(XTRAP_comb14, by="pt")
#Add log2 and L-ratio
t1 <- rep(marknames,each=4)
t2 <- rep(c("_Lr1","_Lr2","_Lr3","_Lr4"),times=length(marknames))
newmark <- paste0(t1,t2)
XTRAP_comb4 <- XTRAP_comb3 %>% mutate_at(vars(matches("Baseline")),funs("log2"=log2))
XTRAP_comb5 <- bind_cols(lapply(paste0(marknames,"_Final"),function(x){
  val1<-log2(XTRAP_comb4%>%select(paste0(x,"_C4D1")))-log2(XTRAP_comb4%>%select(paste0(x,"_Baseline"))); 
  val2<-log2(XTRAP_comb4%>%select(paste0(x,"_Progression")))-log2(XTRAP_comb4%>%select(paste0(x,"_Baseline")));
  val3<-log2(XTRAP_comb4%>%select(paste0(x,"_30DayPost")))-log2(XTRAP_comb4%>%select(paste0(x,"_Baseline")));
  val4<-log2(XTRAP_comb4%>%select(paste0(x,"_30DayPost")))-log2(XTRAP_comb4%>%select(paste0(x,"_Progression")))
  return(data.frame(val1,val2,val3,val4))
  })) %>% 
 `colnames<-`(newmark) %>% cbind(XTRAP_comb4) %>% 
  select(names(XTRAP_comb4),newmark)
@


\section{Save}
\begin{frame}[fragile]{Save}
Save the file for later analysis.
<<save, size= 'tiny',message = FALSE, warning = FALSE>>=
save(XTRAP_comb5,file=paste0(savdir,"XTRAP_combo.RData"))  #Save
write.csv(XTRAP_comb5,paste0(savdir,"XTRAP_combo.csv"))   #Save
@
\end{frame}

\section{Check Replicates}
\begin{frame}[fragile]{Check Replicates}
Check replicates:
<<rep1, size= 'tiny', message = FALSE, warning = FALSE>>=
rep<-function(dat,marker,cycle){
  temp<-dat %>% filter(grepl(paste0(cycle,"$"),`Sample Des`))
  var1<-paste0(marker,"_Rep1")
  var2<-paste0(marker,"_Rep2")
  ggplot(temp,aes(x=unlist(temp[,var1]),y=unlist(temp[,var2])))+geom_point()+geom_abline(intercept=0,slope=1)+labs(x=paste0(marker,"_Rep1"),y=paste0(marker,"_Rep2"),title=paste(cycle,":",marker))
  }

plotfour<-function(dat,i,cycle){
  if(i<13){
    grid.arrange(rep(dat,marknames[4*(i-1)+1],cycle),rep(dat,marknames[4*(i-1)+2],cycle),rep(dat,marknames[4*(i-1)+3],cycle),rep(dat,marknames[4*i],cycle),ncol=2,nrow=2)
  }else{
    grid.arrange(rep(dat,marknames[4*(i-1)+1],cycle),nrow=1)
  }
}
@
\end{frame}

<<repplot1, echo=FALSE, include = FALSE>>=
for(j in 1:13){
 plotfour(mydata2,j,"Baseline")
}
@
<<repplot2, echo=FALSE, include = FALSE>>=
for(j in 1:13){
 plotfour(mydata2,j,"C4D1")
}
@
<<repplot3, echo=FALSE, include = FALSE>>=
for(j in 1:13){
 plotfour(mydata2,j,"Progression")
}
@
<<repplot4, echo=FALSE, include = FALSE>>=
for(j in 1:13){
 plotfour(mydata2,j,"30Day Post")
}
@

<<repplot11, echo=FALSE,results='asis'>>=
for(m in 1:13){
cat("\\begin{frame}[fragile]{Check Replicates: Baseline \n}")
  cat(paste("\\includegraphics[width=\\textwidth,",
            "height=\\textheight,keepaspectratio]{figure/beamer-repplot1-",
            m, ".pdf}\n", sep=""))
cat("\\end{frame}\n")
}
@
<<repplot22, echo=FALSE,results='asis'>>=
for(m in 1:13){
cat("\\begin{frame}[fragile]{Check Replicates: C4D1 \n}")
  cat(paste("\\includegraphics[width=\\textwidth,",
            "height=\\textheight,keepaspectratio]{figure/beamer-repplot2-",
            m, ".pdf}\n", sep=""))
cat("\\end{frame}\n")
}
@
<<repplot33, echo=FALSE,results='asis'>>=
for(m in 1:13){
cat("\\begin{frame}[fragile]{Check Replicates: Progression \n}")
  cat(paste("\\includegraphics[width=\\textwidth,",
            "height=\\textheight,keepaspectratio]{figure/beamer-repplot3-",
            m, ".pdf}\n", sep=""))
cat("\\end{frame}\n")
}
@
<<repplot44, echo=FALSE,results='asis'>>=
for(m in 1:13){
cat("\\begin{frame}[fragile]{Check Replicates: 30 Days Post \n}")
  cat(paste("\\includegraphics[width=\\textwidth,",
            "height=\\textheight,keepaspectratio]{figure/beamer-repplot4-",
            m, ".pdf}\n", sep=""))
cat("\\end{frame}\n")
}
@

\end{document}
